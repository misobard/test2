﻿namespace お試し2
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.yesterdayText = new System.Windows.Forms.MaskedTextBox();
            this.今日のおすすめ睡眠時間 = new System.Windows.Forms.Label();
            this.averageText = new System.Windows.Forms.MaskedTextBox();
            this.todayText = new System.Windows.Forms.MaskedTextBox();
            this.sleepButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.morningText = new System.Windows.Forms.MaskedTextBox();
            this.commentLabel = new System.Windows.Forms.Label();
            this.MorningAfternoonLabel = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "昨日の就寝時間";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "どれくらい寝たいか";
            // 
            // yesterdayText
            // 
            this.yesterdayText.Location = new System.Drawing.Point(155, 10);
            this.yesterdayText.Mask = "00";
            this.yesterdayText.Name = "yesterdayText";
            this.yesterdayText.Size = new System.Drawing.Size(100, 19);
            this.yesterdayText.TabIndex = 4;
            // 
            // 今日のおすすめ睡眠時間
            // 
            this.今日のおすすめ睡眠時間.AutoSize = true;
            this.今日のおすすめ睡眠時間.Location = new System.Drawing.Point(334, 41);
            this.今日のおすすめ睡眠時間.Name = "今日のおすすめ睡眠時間";
            this.今日のおすすめ睡眠時間.Size = new System.Drawing.Size(127, 12);
            this.今日のおすすめ睡眠時間.TabIndex = 5;
            this.今日のおすすめ睡眠時間.Text = "今日のおすすめ睡眠時間";
            // 
            // averageText
            // 
            this.averageText.Location = new System.Drawing.Point(155, 54);
            this.averageText.Mask = "00";
            this.averageText.Name = "averageText";
            this.averageText.Size = new System.Drawing.Size(100, 19);
            this.averageText.TabIndex = 6;
            // 
            // todayText
            // 
            this.todayText.Location = new System.Drawing.Point(361, 68);
            this.todayText.Mask = "00";
            this.todayText.Name = "todayText";
            this.todayText.Size = new System.Drawing.Size(100, 19);
            this.todayText.TabIndex = 7;
            // 
            // sleepButton
            // 
            this.sleepButton.Location = new System.Drawing.Point(172, 268);
            this.sleepButton.Name = "sleepButton";
            this.sleepButton.Size = new System.Drawing.Size(75, 40);
            this.sleepButton.TabIndex = 8;
            this.sleepButton.Text = "計算";
            this.sleepButton.UseVisualStyleBackColor = true;
            this.sleepButton.Click += new System.EventHandler(this.SleepButtonClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "起きる時間";
            // 
            // morningText
            // 
            this.morningText.Location = new System.Drawing.Point(155, 97);
            this.morningText.Mask = "00";
            this.morningText.Name = "morningText";
            this.morningText.Size = new System.Drawing.Size(100, 19);
            this.morningText.TabIndex = 10;
            // 
            // commentLabel
            // 
            this.commentLabel.AutoSize = true;
            this.commentLabel.Location = new System.Drawing.Point(26, 183);
            this.commentLabel.Name = "commentLabel";
            this.commentLabel.Size = new System.Drawing.Size(38, 12);
            this.commentLabel.TabIndex = 13;
            this.commentLabel.Text = "コメント";
            // 
            // MorningAfternoonLabel
            // 
            this.MorningAfternoonLabel.AutoSize = true;
            this.MorningAfternoonLabel.Location = new System.Drawing.Point(467, 71);
            this.MorningAfternoonLabel.Name = "MorningAfternoonLabel";
            this.MorningAfternoonLabel.Size = new System.Drawing.Size(35, 12);
            this.MorningAfternoonLabel.TabIndex = 14;
            this.MorningAfternoonLabel.Text = "日にち";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(326, 265);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 47);
            this.clearButton.TabIndex = 15;
            this.clearButton.Text = "クリア";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButtonClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(261, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 18;
            this.label4.Text = "時";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(261, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 12);
            this.label5.TabIndex = 19;
            this.label5.Text = "時";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(261, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 20;
            this.label6.Text = "時";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(513, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 21;
            this.label7.Text = "時";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 337);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.MorningAfternoonLabel);
            this.Controls.Add(this.commentLabel);
            this.Controls.Add(this.morningText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.sleepButton);
            this.Controls.Add(this.todayText);
            this.Controls.Add(this.averageText);
            this.Controls.Add(this.今日のおすすめ睡眠時間);
            this.Controls.Add(this.yesterdayText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox yesterdayText;
        private System.Windows.Forms.Label 今日のおすすめ睡眠時間;
        private System.Windows.Forms.MaskedTextBox averageText;
        private System.Windows.Forms.MaskedTextBox todayText;
        private System.Windows.Forms.Button sleepButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox morningText;
        private System.Windows.Forms.Label commentLabel;
        private System.Windows.Forms.Label MorningAfternoonLabel;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

