﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace お試し2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            // サイズ変更可能な通常のウィンドウ
            this.FormBorderStyle = FormBorderStyle.Sizable;

            // フォームに自動スクロールバーを付ける
            this.AutoScroll = true;

            InitializeComponent();
        }

        private int PutIn;
        private int Putin2;

        private int yesterday;
        private int sleep;
        private int morning;

        private void SleepButtonClicked(object sender, EventArgs e)
        {
            bool yesterdaysuccess = int.TryParse(this.yesterdayText.Text, out yesterday);
            bool sleepSuccess = int.TryParse(this.averageText.Text, out sleep);
            bool morningsuccess = int.TryParse(this.morningText.Text, out morning);

            if (yesterdaysuccess & sleepSuccess && morningsuccess)
            {

                PutIn = 24 - sleep + morning;
                Putin2 = PutIn;

                if (PutIn > 24)
                {
                    PutIn -= 24;
                }
                int averageSleep = (int)(PutIn);
                this.todayText.Text = averageSleep.ToString();

                
                if (yesterday == Putin2)
                {
                    this.commentLabel.Text = "十分睡眠をとれて" +
                        "いますこの調子でいきましょう";
                }
                else if (yesterday > Putin2)
                {
                    this.commentLabel.Text = "睡眠時間が足りていないようです。" +
                        "早めに睡眠をとりましょう";
                }
                else if (yesterday < Putin2)
                {
                    this.commentLabel.Text = "十分に睡眠がとれているようですね。" +
                        "余裕があるので他の時間に回してもいいかもしれません";
                }

                if (Putin2 > 24)
                {
                    MorningAfternoonLabel.Text = "明日の";
                }
                else
                {
                    MorningAfternoonLabel.Text = "今日の";
                }
            }
        }
        

        private void clearButtonClick(object sender, EventArgs e)
        { yesterdayText.Clear();
            averageText.Clear();
            morningText.Clear();
            todayText.Clear();
            commentLabel.Text="コメント";
            MorningAfternoonLabel.Text="日にち";

        }
    }
   }
    

        
